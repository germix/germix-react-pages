# Germix React Pages

## About

Germix react page components

## Installation

```bash
npm install @germix/germix-react-pages
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
