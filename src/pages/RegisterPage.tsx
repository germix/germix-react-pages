import React from 'react';
import { TextField, Button, FormGroup, Alert } from '@germix/germix-react-core';
import PagePanel from './PagePanel';

interface Props
{
    logo?: React.ReactElement;
    title,
    background?,

    message,
    disabled,
    processing,

    emailInput,
    emailLabel : string,
    emailValue : string,
    passwordInput,
    passwordLabel : string,
    passwordValue : string,
    confirmPasswordInput,
    confirmPasswordLabel : string,
    confirmPasswordValue : string,

    buttonLabel,
    termsOfService,

    onSubmit(),
    onChangeEmail(e),
    onChangePassword(e),
    onChangeConfirmPassword(e),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class RegisterPage extends React.Component<Props,State>
{
    form;
    submit;

    render()
    {
        return (
<PagePanel
    logo={this.props.logo}
    title={this.props.title}
    background={this.props.background}
>
<>

    <form ref={elem => this.form = elem} onSubmit={(event) =>
        {
            event.preventDefault();
            this.props.onSubmit();
        }}>

        <FormGroup>
            <label>{this.props.emailLabel}</label>
            <TextField
                required
                disabled={this.props.disabled}
                innerRef={this.props.emailInput}
                type="email"
                value={this.props.emailValue}
                onChange={(e) => { this.props.onChangeEmail(e); }} />
        </FormGroup>

        <FormGroup>
            <label>{this.props.passwordLabel}</label>
            <TextField
                required
                type="password"
                disabled={this.props.disabled}
                innerRef={this.props.passwordInput}
                value={this.props.passwordValue}
                onChange={(e) => { this.props.onChangePassword(e); }} />
        </FormGroup>

        <FormGroup>
            <label>{this.props.confirmPasswordLabel}</label>
            <TextField
                required
                type="password"
                disabled={this.props.disabled}
                innerRef={this.props.confirmPasswordInput}
                value={this.props.confirmPasswordValue}
                onChange={(e) => { this.props.onChangeConfirmPassword(e); }} />
        </FormGroup>

        <input ref={elem => this.submit = elem} type="submit" style={{display: 'none'}} disabled={this.props.disabled}/>
    </form>
</>
<>
    <FormGroup>
        <Button disabled={this.props.disabled} processing={this.props.processing} color="primary" fullwidth uppercase onClick={ () =>
        {
            this.submit.click();
        }}
        >{this.props.buttonLabel}</Button>
    </FormGroup>
    { this.props.termsOfService &&
        <FormGroup>
            { this.props.termsOfService }
        </FormGroup>
    }

    {this.props.message &&
        <FormGroup>
            <Alert color="danger">{this.props.message}</Alert>
        </FormGroup>
    }
</>
</PagePanel>
        );
    }
};
export default RegisterPage;
