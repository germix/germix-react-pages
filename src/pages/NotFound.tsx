import React from 'react';
import { Button } from '@germix/germix-react-core';

interface Props
{
    image,
    message,
    button,
    onClick(event),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NotFound extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="not-found">
    <img src={this.props.image}/>
    <p>{this.props.message}</p>
    <Button color="primary" onClick={this.props.onClick}>{this.props.button}</Button>
</div>
        );
    }
}
export default NotFound;
