import React from 'react';

import PagePanel from './PagePanel';

interface Props
{
    title,
    background?,
    icon,
    message,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class UnderMaintenancePage extends React.Component<Props,State>
{
    render()
    {

        return (
<PagePanel
    title={this.props.title}
    background={this.props.background}
>
<>
    <div className="under-maintenance-message">
        <p>{this.props.message}</p>
    </div>
</>
<>
    <div className="under-maintenance-icon">
        <i className={this.props.icon}></i>
    </div>
</>
</PagePanel>
        );
    }
};
export default UnderMaintenancePage;
