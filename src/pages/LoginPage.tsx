import React, { PropsWithChildren } from 'react';
import { TextField, Button, CheckBox, Alert, FormGroup } from '@germix/germix-react-core';

import PagePanel from './PagePanel';

interface Props extends PropsWithChildren<any>
{
    logo?: React.ReactElement;
    title : string,
    background? : string,

    message : string,
    disabled : boolean,
    processing : boolean,

    buttonLabel : string,
    emailInput,
    emailLabel : string,
    emailValue : string,
    passwordInput,
    passwordLabel : string,
    passwordValue : string,
    rememberMeLabel? : string,
    rememberMeState? : boolean,
    forgotPasswordLabel? : string,
    forgotPasswordRoute? : string,

    onSubmit(),
    onChangeEmail(e),
    onChangePassword(e),
    onForgotPassword?(),
    onChangeRememberMeState?(checked),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class LoginPage extends React.Component<Props,State>
{
    form;
    submit;

    render()
    {
        return (
<PagePanel
    logo={this.props.logo}
    title={this.props.title}
    background={this.props.background}
>
<>
    <form ref={elem => this.form = elem} onSubmit={(event) =>
    {
        event.preventDefault();
        this.props.onSubmit();
    }}>
        <FormGroup>
            <label>{this.props.emailLabel}</label>
            <TextField
                required
                disabled={this.props.disabled}
                innerRef={this.props.emailInput}
                value={this.props.emailValue}
                onChange={(e) => { this.props.onChangeEmail(e); }} />
        </FormGroup>

        <FormGroup>
            <label>{this.props.passwordLabel}</label>
            <TextField
                required
                type="password"
                disabled={this.props.disabled}
                innerRef={this.props.passwordInput}
                value={this.props.passwordValue}
                onChange={(e) => { this.props.onChangePassword(e); }} />
        </FormGroup>

        <input ref={elem => this.submit = elem} type="submit" style={{display: 'none'}} disabled={this.props.disabled}/>
    </form>
</>
<>
    <FormGroup>
        <div className="row">
            <div className="col">
                { (this.props.rememberMeLabel && this.props.onChangeRememberMeState) &&
                    <CheckBox disabled={this.props.disabled} checked={this.props.rememberMeState} onChange={this.props.onChangeRememberMeState}>{this.props.rememberMeLabel}</CheckBox>
                }
            </div>
            <div className="col" style={{
                flexGrow: 1,
                display: 'flex',
                justifyContent: 'flex-end',
            }}>
                <a href={this.props.forgotPasswordRoute} onClick={(e) => {
                    e.preventDefault();
                    if(!this.props.disabled && this.props.onForgotPassword)
                    {
                        this.props.onForgotPassword();
                    }
                }}>{this.props.forgotPasswordLabel}</a>
            </div>
        </div>
    </FormGroup>
    <FormGroup>
        <Button disabled={this.props.disabled} processing={this.props.processing} color="primary" fullwidth uppercase onClick={ () =>
        {
            this.submit.click();
        }}
        >{this.props.buttonLabel}</Button>
    </FormGroup>
    {this.props.message &&
        <FormGroup>
            <Alert color="danger">{this.props.message}</Alert>
        </FormGroup>
    }
</>
</PagePanel>
        );
    }
};
export default LoginPage;
