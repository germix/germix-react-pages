import React from 'react';

/**
 * @author Germán Martínez
 */
class FullPage extends React.Component
{
    render()
    {
        return (
<div className="full-page">
    { this.props.children }
</div>
        )
    }
};
export default FullPage;
