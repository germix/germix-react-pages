import React from 'react';
import { TextField, Button, Alert, FormGroup } from '@germix/germix-react-core';
import PagePanel from './PagePanel';

interface Props
{
    logo?: React.ReactElement;
    title : string,
    background? : string,

    message : string,
    disabled : boolean,
    processing : boolean,

    buttonLabel : string,

    emailInput,
    emailLabel : string,
    emailValue : string,

    onSubmit(),
    onChangeEmail(e),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class ForgotPasswordPage extends React.Component<Props,State>
{
    form;
    submit;

    render()
    {
        return (
<PagePanel
    logo={this.props.logo}
    title={this.props.title}
    background={this.props.background}
>
<>
    <form ref={elem => this.form = elem} onSubmit={(event) =>
    {
        event.preventDefault();
        this.props.onSubmit();
    }}>
        <FormGroup>
            <label>{this.props.emailLabel}</label>
            <TextField
                type="email"
                required
                disabled={this.props.disabled}
                innerRef={this.props.emailInput}
                value={this.props.emailValue}
                onChange={(e) => { this.props.onChangeEmail(e); }} />
        </FormGroup>

        <input ref={elem => this.submit = elem} type="submit" style={{display: 'none'}} disabled={this.props.disabled}/>
    </form>
</>
<>
    <FormGroup>
        <Button disabled={this.props.disabled} processing={this.props.processing} color="primary" fullwidth uppercase onClick={ () =>
        {
            this.submit.click();
        }}
        >{this.props.buttonLabel}</Button>
    </FormGroup>
    {this.props.message &&
        <FormGroup>
            <Alert color="danger">{this.props.message}</Alert>
        </FormGroup>
    }
</>
</PagePanel>
        );
    }
};
export default ForgotPasswordPage;
