import React from 'react';

interface Props
{
    logo?: React.ReactElement;
    title : string,
    background? : string,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class PagePanel extends React.Component<Props,State>
{
    render()
    {
        let style:any =
        {
            background: undefined,
            backgroundSize: undefined,
        };
        if(this.props.background)
        {
            style.background = "url('" + this.props.background + "') no-repeat center center fixed";
            style.backgroundSize = 'cover';
        }
        const children = React.Children.toArray(this.props.children);
        return (
<div className="full-page" style={style}>
    <div className="page-panel">
        { this.props.logo }
        <div className="page-panel-header">
            <div className="page-panel-title">{this.props.title}</div>
        </div>
        <div className="page-panel-body">
            { children.length > 0 && children[0] }
        </div>
        <div className="page-panel-footer">
            { children.length > 1 && children[1] }
        </div>
    </div>
</div>
        );
    }
};
export default PagePanel;
